import torch
import torch.nn

#import VolumeRenderer
torch.ops.load_library("./MyLibrary.dll") # ${BINDING_NAME} in CMakeLists
    
example_input = torch.rand(1,1,3,3)

scripted_module = torch.jit.load("test.pt")

print(example_input)
print(scripted_module(example_input, example_input))