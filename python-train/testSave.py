
# has to be executed with working directory set to bin/
# or whereever 

import torch
import torch.nn

#import VolumeRenderer
print("Import library")
torch.ops.load_library("./MyLibrary.dll") # ${BINDING_NAME} in CMakeLists
print(torch.ops.mylibrary.add_cpu)
print(torch.ops.mylibrary.add_gpu)

device = torch.device('cuda')

print("Create module")
class MyModule(torch.nn.Module):
    def __init__(self):
        super().__init__()
        #self.device = torch.device('cuda')
        self.op = torch.ops.mylibrary.add_gpu
    
    def forward(self, a, b):
        device = torch.device('cuda')
        a = a.to(device)
        b = b.to(device)
        return self.op(a, b)
   
scripted_module = torch.jit.script(MyModule())

print("Save module")
torch.jit.save(scripted_module, "test.pt")