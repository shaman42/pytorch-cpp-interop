cmake_minimum_required(VERSION 3.10)
project(VolumeRendererLib)

####################################
# GENERAL THIRD-PARTY DEPENDENCIES
####################################

# CUDA is always required
find_package(CUDA REQUIRED)
if(COMMAND CUDA_SELECT_NVCC_ARCH_FLAGS)
	CUDA_SELECT_NVCC_ARCH_FLAGS(ARCH_FLAGS Auto)
	LIST(APPEND CUDA_NVCC_FLAGS ${ARCH_FLAGS})
endif()

# PYTHON
find_package(PythonInterp 3.5 REQUIRED)
find_package(PythonLibs 3.5 REQUIRED)
get_filename_component(PYTHON_DIRECTORY ${PYTHON_EXECUTABLE} DIRECTORY)

####################################
# PYTORCH
####################################
# find installation path
if(NOT DEFINED ${TORCH_PATH})
	# query torch path from python
	execute_process(COMMAND CMD /c python -c "import torch.utils.cpp_extension; print(torch.utils.cpp_extension.include_paths()[0])" OUTPUT_VARIABLE TORCH_FIRST_INCLUDE_DIR)
	get_filename_component(TORCH_ROOT ${TORCH_FIRST_INCLUDE_DIR}/../ ABSOLUTE)
	set(TORCH_PATH "${TORCH_ROOT}" CACHE FILEPATH "path to pytorch in the python installation")
	if(NOT (EXISTS ${TORCH_PATH}))
		message( FATAL_ERROR "Pytorch not found, is it not installed in the python distribution ${PYTHON_DIRECTORY}?")
	else()
		message(STATUS "Torch found at ${TORCH_PATH}")
	endif()
endif(NOT DEFINED ${TORCH_PATH})
# ask Torch's CMake configuration
set(TORCH_CONFIG_PATH "${TORCH_PATH}/share/cmake/Torch" CACHE FILEPATH "possible path where TorchConfig.cmake is located")
list(APPEND CMAKE_PREFIX_PATH ${TORCH_CONFIG_PATH})
find_package(Torch REQUIRED)
# get libraries (hard coded)
set(TORCH_LIBRARY_NAMES
	c10 c10_cuda caffe2_nvrtc torch_python _C)
set(TORCH_LIBRARIES ${TORCH_LIBRARY})
FOREACH(LIB_NAME ${TORCH_LIBRARY_NAMES})
  set(LIB_VAR "TORCH_LIB_${LIB_NAME}") # Name of the variable which stores result of the search
  FIND_LIBRARY(${LIB_VAR} ${LIB_NAME} PATHS ${TORCH_PATH}/lib)
  list(APPEND TORCH_LIBRARIES ${${LIB_VAR}})
ENDFOREACH()
message(STATUS "Torch: full library list: ${TORCH_LIBRARIES}")
# copy shared library to bin/
file(MAKE_DIRECTORY bin)
file(GLOB TORCH_SHARED_LIBRARIES
	${TORCH_PATH}/lib/*${CMAKE_SHARED_LIBRARY_SUFFIX})
message(STATUS "Torch: shared libraries to copy: ${TORCH_SHARED_LIBRARIES}")
file(COPY ${TORCH_SHARED_LIBRARIES} DESTINATION ${CMAKE_SOURCE_DIR}/bin/)
# get include directories
set(TORCH_INCLUDE_DIR "${TORCH_PATH}/include;${TORCH_PATH}/include/torch/csrc/api/include" CACHE FILEPATH "include directory for the pytorch headers")
message(STATUS "Torch: include directories: ${TORCH_INCLUDE_DIR}")

####################################
# THE LIBRARY
####################################
add_subdirectory(cpp-library)

####################################
# PYTHON APPLICATION
####################################
add_subdirectory(python-train)

####################################
# TEST APPLICATION
# depend on the library
####################################
add_subdirectory(cpp-inference)