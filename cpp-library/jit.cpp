#include <torch/script.h>

#include "lib.h"

#define TORCH_EXTENSION_NAME mylibrary

#define xstr(s) str(s)
#define str(s) #s

static auto registry = torch::jit::RegisterOperators()
.op(xstr(TORCH_EXTENSION_NAME) "::add_cpu", &custom_add_cpu)
.op(xstr(TORCH_EXTENSION_NAME) "::add_gpu", &custom_add_gpu);