#pragma once

#include <torch/types.h>
//#include <torch/extension.h> //for the python bindings, doesn't work with nvcc
//similarly, torch/all.h is also too much

#ifdef BUILD_MAIN_LIB
#define MY_API C10_EXPORT
#else
#define MY_API C10_IMPORT
#endif

MY_API torch::Tensor custom_add_cpu(torch::Tensor a, torch::Tensor b);
MY_API torch::Tensor custom_add_gpu(torch::Tensor a, torch::Tensor b);
