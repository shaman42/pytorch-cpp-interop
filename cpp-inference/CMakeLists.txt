cmake_minimum_required(VERSION 3.10)

# your configuration
set(INFERENCE_FILES
	main.cpp
	)
set(INFERENCE_NAME TestInference CACHE INTERNAL "target name for the inference")
	
# create the executable
cuda_add_executable(${INFERENCE_NAME}
	${INFERENCE_FILES}
	)
target_include_directories(${INFERENCE_NAME}
	PRIVATE ${TORCH_INCLUDE_DIR} ${LIBRARY_INCLUDE_DIR}
	)
target_link_libraries(${INFERENCE_NAME}
	${LIBRARY_NAME})
set_property(TARGET ${INFERENCE_NAME} PROPERTY CXX_STANDARD 17)
add_custom_command(TARGET ${INFERENCE_NAME}
	POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/bin
	COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${INFERENCE_NAME}> ${CMAKE_SOURCE_DIR}/bin/${INFERENCE_NAME}${CMAKE_EXECUTABLE_SUFFIX}

	COMMENT "Copies the executable to bin/"
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/..
	
	VERBATIM
	)
set_property(TARGET ${INFERENCE_NAME} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")