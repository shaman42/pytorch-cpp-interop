#include <torch/script.h>
#include <iostream>

#include <lib.h>

int main(int argc, const char* argv[])
{
	//create example tensors
	torch::Tensor t1 = torch::randn({ 4, 8 }, c10::TensorOptions(c10::ScalarType::Float));
	torch::Tensor t2 = torch::randn({ 4, 8 }, c10::TensorOptions(c10::ScalarType::Float));
	std::cout << "t1:\n" << t1 << std::endl;
	std::cout << "t2:\n" << t1 << std::endl;
	std::vector<torch::jit::IValue> inputs;
	inputs.push_back(t1);
	inputs.push_back(t2);

	//run cpu op
	torch::Tensor output1 = custom_add_cpu(t1, t2);
	std::cout << "output1:\n" << output1 << std::endl;

	//deserialize scripted module
	torch::jit::script::Module module = torch::jit::load("test.pt");
	torch::Tensor output2 = module.forward(inputs).toTensor();
	std::cout << "output2:\n" << output2 << std::endl;
}