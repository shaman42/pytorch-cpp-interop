# Interop-project between C++/CUDA and PyTorch

It consists of three projects:
 - cpp-library: a shared library written in C++/CUDA that defines and exposes PyTorch operations
 - python-train: python training code, shows how to import the ops defined in cpp-library and how to export a trained module in such a way that it can be imported in C++
 - cpp-inference: C++ executable that demonstrates how to call the ops from cpp-library directly and how to load and evaluate the model created by python-train
 
### Requirements

 - CUDA
 - Python > 3.6
 - PyTorch > 1.1
 
Tested with CUDA 10.0, Python 3.6, PyTorch 1.2, Windows 10

### Basic Concepts
Library:
```
\\lib.h (the header)

#include <torch/types.h>

#ifdef BUILD_MAIN_LIB
#define MY_API C10_EXPORT
#else
#define MY_API C10_IMPORT
#endif

MY_API torch::Tensor custom_add_gpu(torch::Tensor a, torch::Tensor b);

\\lib_cuda.cu (the implementation)

#include "lib.h"

#define CHECK_CUDA(x) AT_ASSERTM(x.type().is_cuda(), #x " must be a CUDA tensor")
#define CHECK_CONTIGUOUS(x) AT_ASSERTM(x.is_contiguous(), #x " must be contiguous")
#define CHECK_INPUT(x) CHECK_CUDA(x); CHECK_CONTIGUOUS(x)

torch::Tensor custom_add_gpu(torch::Tensor a, torch::Tensor b)
{
	CHECK_INPUT(a);
	CHECK_INPUT(b);
	torch::Tensor foo = a + b + 1;
	return foo;
}

\\jit.cpp (the binding)

#include <torch/script.h>
#include "lib.h"

#define TORCH_EXTENSION_NAME "mylibrary"
static auto registry = torch::jit::RegisterOperators()
.op(TORCH_EXTENSION_NAME) "::add_gpu", &custom_add_gpu);
```

Python side, generating and saving the model
```
import torch
import torch.nn

print("Import library")
torch.ops.load_library("./MyLibrary.dll")
print(torch.ops.mylibrary.add_gpu)

print("Create module")
class MyModule(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.op = torch.ops.mylibrary.add_gpu
    
    def forward(self, a, b):
        device = torch.device('cuda')
        a = a.to(device)
        b = b.to(device)
        return self.op(a, b)
scripted_module = torch.jit.script(MyModule())

print("Save module")
torch.jit.save(scripted_module, "test.pt")
```

C++ Inference: use ops from the library and call the saved module
```
#include <torch/script.h>
#include <iostream>
#include <lib.h>
int main(int argc, const char* argv[])
{
	//create example tensors
	torch::Tensor t1 = torch::randn({ 4, 8 }, c10::TensorOptions(c10::ScalarType::Float)).to(c10::kCUDA);
	torch::Tensor t2 = torch::randn({ 4, 8 }, c10::TensorOptions(c10::ScalarType::Float)).to(c10::kCUDA);
	std::cout << "t1:\n" << t1 << std::endl;
	std::cout << "t2:\n" << t1 << std::endl;
	std::vector<torch::jit::IValue> inputs;
	inputs.push_back(t1);
	inputs.push_back(t2);

	//run operation from the library
	torch::Tensor output1 = custom_add_gpu(t1, t2);
	std::cout << "output1:\n" << output1 << std::endl;

	//deserialize scripted module and run it
	torch::jit::script::Module module = torch::jit::load("test.pt");
	torch::Tensor output2 = module.forward(inputs).toTensor();
	std::cout << "output2:\n" << output2 << std::endl;
}
```

## Alternative Way
The method described here is rather invasive. It requires that your library uses PyTorch tensors as input and output.
If you want to use an existing library in PyTorch, the interop can also be done with the help of Python's ctypes (https://docs.python.org/3/library/ctypes.html).

The library has to compared as a shared library and all functions that should be callable from Python have to be enclosed in `extern "C"`.
Example
```
//C++:
extern "C"
{
    __declspec(dllexport) void setParameter(const char* cmd, const char* value) {
        ....
    }
}
```
In Python, this code can be loaded and accessed in the following way
```
# Python
import ctypes
lib = ctypes.cdll.LoadLibrary("path-to-the-library.dll")
lib.setParameter.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
lib.setParameter("parameter", "value")
```

For the Pytorch interop, it is possible to send CUDA tensors via pointer to C++:
```
//C++:
extern "C"
{
    __declspec(dllexport) void compute(CUdeviceptr targetTensor) {
        ....
    }
}
```
```
# Python
lib.compute.argtypes = [ctypes.c_ulonglong]
tensor = # pre-initialized tensor on the GPU with known sizes
lib.compute(ctypes.c_ulonglong(tensor.data_ptr()))
```